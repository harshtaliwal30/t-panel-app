import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';


String fileType = '';
File file;
String fileName = '';
String fileExtension;
String downloadUrl;
Reference storageReference;
UploadTask uploadTask;
List extensionArray = [
  ['.jpg', '.png', '.jpeg', '.gif', '.tiff', '.tif', '.bmp', '.esp'],
  ['.mpeg-2', '.mp4', '.mkv', '.avchd', '.avi,', '.wmv', '.mov'],
  ['.pdf']
];
var documentFields;
double filesize;
double sizeInMb;
double fileLimit;
bool isSubmitted = false;
bool filepick = false;
List notificationReceiver;
GlobalKey<FormState> formKey = new GlobalKey<FormState>();
bool isReviewed = false;
var submittedAt;
var submitDescriptionController = TextEditingController();
String submitDescription;
