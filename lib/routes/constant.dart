const String homeRoute = '/home';
const String splashRoute = '/';
const String taskCategoryRoute = '/task-category';
const String dashboardRoute = '/dashboard';
const String createPanelRoute = '/create-panel';
const String bottomPanelRoute = '/bottom-panel';
const String joinPanelRoute = '/join-panel';
const String addMemberRoute = '/add-member';
const String settingsRoute = '/settings';
const String loginRoute = '/login';
const String chatRoute = '/chat';
const String accountProfileRoute = '/account-profile';
const String workProfileRoute = '/work-profile';
const String submitTaskRoute = '/submit-task';
