import 'package:flutter/material.dart';
import 'package:t_panel_app/main.dart';
import 'package:t_panel_app/routes/constant.dart';
import 'package:t_panel_app/screens/add_member_screen.dart';
import 'package:t_panel_app/screens/bottombar/bottom_panel.dart';
import 'package:t_panel_app/screens/chat_screen.dart';
import 'package:t_panel_app/screens/dashboard.dart';
import 'package:t_panel_app/screens/forms/auth_screen.dart';
import 'package:t_panel_app/screens/forms/create_panel.dart';
import 'package:t_panel_app/screens/forms/join_panel.dart';
import 'package:t_panel_app/screens/individual/account_profile_screen.dart';
import 'package:t_panel_app/screens/individual/profile.dart';
import 'package:t_panel_app/screens/settings.dart';
import 'package:t_panel_app/screens/tasks/category_task.dart';
import 'package:t_panel_app/screens/tasks/submit_task_screen.dart';

class Routers {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashRoute:
        return MaterialPageRoute(builder: (_) => Splash());
      case loginRoute:
        return MaterialPageRoute(builder: (_) => AuthScreen());
      case dashboardRoute:
        return MaterialPageRoute(builder: (_) => Dashboard());
      case createPanelRoute:
        return MaterialPageRoute(builder: (_) => CreatePanel());
      case joinPanelRoute:
        return MaterialPageRoute(builder: (_) => JoinPanel());
      case addMemberRoute:
        return MaterialPageRoute(builder: (_) => AddMember());
      case bottomPanelRoute:
        return MaterialPageRoute(builder: (_) => BottomPanel());
      case accountProfileRoute:
        var data = settings.arguments;
        return MaterialPageRoute(builder: (_) => AccountProfileScreen(data));
      case workProfileRoute:
        return MaterialPageRoute(builder: (_) => ProfilePage());
      case submitTaskRoute:
        return MaterialPageRoute(builder: (_) => SubmitTask());
      case settingsRoute:
        var data = settings.arguments;
        return MaterialPageRoute(builder: (_) => Settings(data));
      case chatRoute:
        var data = settings.arguments;
        return MaterialPageRoute(builder: (_) => ChatScreen(data));
      case taskCategoryRoute:
        var data = settings.arguments;
        return MaterialPageRoute(builder: (_) => CategoryTask(data));
      case homeRoute:
        return MaterialPageRoute(builder: (_) => MyHomePage());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ),
        );
    }
  }
}
