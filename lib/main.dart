import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:t_panel_app/routes/constant.dart';
import 'package:t_panel_app/routes/router.dart';
import 'package:t_panel_app/screens/forms/auth_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:t_panel_app/screens/dashboard.dart';
import 'package:t_panel_app/widgets/auth/confirm_email.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.black, // status bar color
    ));
    return MaterialApp(
      title: 'Flutter Demo',
      // theme: ThemeData(fontFamily: 'Montserrat'),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Routers.generateRoute,
      initialRoute: splashRoute,
    );
  }
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 1,
      navigateAfterSeconds: new MyHomePage(),
      backgroundColor: Color(0xFF121212),
      title: Text(
        "Welcome to T-Panel",
        style: TextStyle(
          color: Colors.grey,
          fontSize: 25,
        ),
      ),
      image: new Image.asset(
        'assets/images/image1.png',
      ),
      loadingText: Text(
        "Loading...",
        style: TextStyle(
          color: Colors.white,
          fontSize: 15,
        ),
      ),
      photoSize: 150.0,
      loaderColor: Colors.blue[900],
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (ctx, userSnapshot) {
          if (userSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (userSnapshot.hasData) {
            return FirebaseAuth.instance.currentUser.emailVerified
                ? Dashboard()
                : ConfirmEmail();
          } else {
            return AuthScreen();
          }
        },
      ),
    );
  }
}
