import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/dashboard_service.dart';

class Panels extends StatefulWidget {
  final DocumentSnapshot document;
  Panels(this.document);

  @override
  _PanelsState createState() => _PanelsState();
}

class _PanelsState extends State<Panels> {
  int assignedTask;
  int ongoingTask;
  int completedTask;
  bool isLoading = false;
  @override
  void initState() {
    DashboardService().getAssignedTask(widget.document.id).then((value) {
      assignedTask = value;
    });
    DashboardService().getOngoingTask(widget.document.id).then((value) {
      ongoingTask = value;
    });
    DashboardService().getCompletedTask(widget.document.id).then((value) {
      completedTask = value;
      setState(() {
        isLoading = true;
      });
    });
    super.initState();
  }

  panelInfoDialog(BuildContext context, String panelDescription) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.grey[900],
        content: ListTile(
          title: Container(
            height: 40,
            color: Colors.black,
            margin: EdgeInsets.only(bottom: 10),
            alignment: Alignment.center,
            child: Text(
              'Description',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          subtitle: Text(
            panelDescription,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'Close',
              style: TextStyle(
                color: Colors.purple,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return !isLoading
        ? SizedBox()
        : Container(
            margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.blueGrey[900],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () async {
                          await DashboardService()
                              .getPanelCreator(widget.document.id);
                          panelId = widget.document.id;
                          panelName = widget.document.data()['panel_name'];
                          panelCode = widget.document.data()['panelId'];
                          panelDescription =
                              widget.document.data()['description'];
                          panelCreatorUsername =
                              widget.document.data()['panel_creator'];
                          panelPurpose = widget.document.data()['purpose'];
                          panelAdmins = widget.document.data()['admins'];
                          panelParticipants =
                              widget.document.data()['participants'];
                          Navigator.of(context).pushNamed('/bottom-panel');
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.document['panel_name'],
                              style: GoogleFonts.baloo(
                                textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                            Text(
                              widget.document['purpose'],
                              style: TextStyle(
                                  color: Colors.grey[400],
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.info_outline,
                          color: Colors.amber,
                        ),
                        onPressed: () {
                          panelInfoDialog(
                            context,
                            widget.document['description'],
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 5, 5),
                  padding: EdgeInsets.all(5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.all(2),
                            child: Column(
                              children: [
                                PopupMenuButton(
                                  child: Text(
                                    'T/A',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 10,
                                    ),
                                  ),
                                  color: Colors.grey[900],
                                  itemBuilder: (context) => [
                                    PopupMenuItem(
                                      child: Text(
                                        'Total tasks assigned to you',
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Color(0XFF879A5B),
                                  ),
                                  height: 35,
                                  width: 35,
                                  child: Center(
                                    child: assignedTask == null
                                        ? Icon(
                                            Icons.rotate_left_outlined,
                                            color: Colors.white,
                                          )
                                        : Text(
                                            assignedTask.toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(2),
                            child: Column(
                              children: [
                                PopupMenuButton(
                                  child: Text(
                                    'T/C',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 10,
                                    ),
                                  ),
                                  color: Colors.grey[900],
                                  itemBuilder: (context) => [
                                    PopupMenuItem(
                                      child: Text(
                                        'Your completed tasks',
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.amber,
                                  ),
                                  height: 35,
                                  width: 35,
                                  child: Center(
                                    child: completedTask == null
                                        ? Icon(
                                            Icons.rotate_left_outlined,
                                            color: Colors.white,
                                          )
                                        : Text(
                                            completedTask.toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(2),
                            child: Column(
                              children: [
                                PopupMenuButton(
                                  child: Text(
                                    'T/O',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 10,
                                    ),
                                  ),
                                  color: Colors.grey[900],
                                  itemBuilder: (context) => [
                                    PopupMenuItem(
                                      child: Text(
                                        'Your ongoing tasks',
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.blueGrey,
                                  ),
                                  height: 35,
                                  width: 35,
                                  child: Center(
                                    child: ongoingTask == null
                                        ? Icon(
                                            Icons.rotate_left_outlined,
                                            color: Colors.white,
                                          )
                                        : Text(
                                            ongoingTask.toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            "Last Task Assigned:",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            DateFormat.yMd()
                                .add_jm()
                                .format(
                                    widget.document['last_updated'].toDate())
                                .toString(),
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 10,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          );
  }
}
