import 'package:flutter/material.dart';

class NoPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RawMaterialButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/create-panel');
                  },
                  child: new Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: new Icon(
                      Icons.add,
                      size: 30.0,
                      color: Colors.white,
                    ),
                  ),
                  shape: new CircleBorder(),
                  fillColor: Color(0xFF2c6bed),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(
                  'Create Project',
                  style: new TextStyle(
                    fontSize: 18.0,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RawMaterialButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/join-panel');
                  },
                  child: new Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: new Icon(
                      Icons.add_chart,
                      size: 30.0,
                      color: Colors.white,
                    ),
                  ),
                  shape: new CircleBorder(),
                  fillColor: Color(0xFF2c6bed),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(
                  'Join Project',
                  style: new TextStyle(fontSize: 18.0, color: Colors.grey),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
