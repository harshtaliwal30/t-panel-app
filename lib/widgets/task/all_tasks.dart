import 'package:avatar_letter/avatar_letter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/notification_service.dart';
import 'package:t_panel_app/services/submit_task_service.dart';
import 'package:t_panel_app/services/task_entry_service.dart';
import 'package:t_panel_app/widgets/task/task_assign.dart';

dynamic taskColor(task) {
  switch (task['status']) {
    case 'Completed':
      return Colors.green;
      break;
    case 'Incompleted':
      return Colors.red;
      break;
    case 'Ongoing':
      return Colors.blue;
      break;
  }
}

taskCategory(category, panelId) {
  switch (category) {
    case 'Completed':
      return TaskEntryService().completedTask();
      break;
    case 'Incompleted':
      return TaskEntryService().incompletedTask();
      break;
    case 'Ongoing':
      return TaskEntryService().ongoingTask();
      break;
  }
}

taskStatus(selectedIndex) {
  switch (selectedIndex) {
    case 0:
      return 'Ongoing';
      break;
    case 1:
      return 'Completed';
      break;
    case 2:
      return 'Incompleted';
      break;
  }
}

accessDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Color(0xFF272727),
      content: ListTile(
        title: Icon(
          Icons.not_interested_outlined,
          color: Colors.grey,
          size: 60,
        ),
        subtitle: Text(
          'Access Denied!',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          color: Colors.purple,
          child: Text(
            'Ok',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    ),
  );
}

deleteDialog(BuildContext context, DocumentSnapshot task) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Color(0xFF272727),
      content: ListTile(
        title: Icon(
          Icons.warning_rounded,
          color: Colors.grey,
          size: 40,
        ),
        subtitle: Text(
          'Alert!\nAre you sure you want to delete.',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          color: Colors.purple,
          child: Text(
            'No',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          color: Colors.purple,
          child: Text(
            'Yes',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () async {
            await TaskEntryService().deleteTask();
            await TaskEntryService().deleteChat();
            NotificationService().pushNotification(
              title: 'Task Deleted',
              msg: task['task_name'] +
                  ' in ' +
                  panelName +
                  ' has been deleted by ' +
                  currentUsername,
              screen: 'notification',
              sendTo: task['assigned_to'],
            );
            Navigator.of(context).pop();
          },
        ),
      ],
    ),
  );
}

Widget showTasks(BuildContext context, DocumentSnapshot task) {
  return Container(
    margin: EdgeInsets.all(10),
    height: 120,
    decoration: BoxDecoration(
      color: Colors.grey[900],
      borderRadius: BorderRadius.circular(10),
      border: Border.all(
        color: Colors.blueGrey[900],
        width: 3,
      ),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 10, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextButton(
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      task['task_name'],
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.balooBhai(
                        textStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          'Status: ',
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: taskColor(task),
                          ),
                          height: 20,
                          width: 90,
                          child: Center(
                            child: Text(
                              task['status'],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        if (task.data().containsKey('isSubmittedLate') &&
                            task['isSubmittedLate'])
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.amber,
                            ),
                            height: 20,
                            width: 90,
                            child: Center(
                              child: Text(
                                'Submitted Late',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.message_rounded),
                    color: Colors.blueGrey,
                    onPressed: () {
                      if (task['assigned_to'].contains(currentUsername) ||
                          task['assigned_by'] == currentUsername ||
                          currentUsername == panelCreatorUsername) {
                        Navigator.of(context).pushNamed(
                          '/chat',
                          arguments: {
                            'documentSnap': task,
                          },
                        );
                      } else {
                        accessDialog(context);
                      }
                    },
                  ),
                  if (currentUsername == panelCreatorUsername ||
                      currentUsername == task['assigned_by'] ||
                      task['assigned_to'].contains(currentUsername))
                    Container(
                      child: PopupMenuButton(
                        icon: Icon(
                          Icons.keyboard_arrow_down_sharp,
                          color: Colors.blueGrey,
                        ),
                        color: Colors.grey[900],
                        itemBuilder: (context) => [
                          if (currentUsername == panelCreatorUsername ||
                              currentUsername == task['assigned_by']) ...[
                            PopupMenuItem(
                              value: 1,
                              child: Container(
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      color: Colors.grey,
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      task['status'] == 'Incompleted'
                                          ? 'Reassign Task'
                                          : 'Edit Task',
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            PopupMenuItem(
                              value: 2,
                              child: Container(
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.delete,
                                      color: Colors.grey,
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      'Delete Task',
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                          PopupMenuItem(
                            value: 3,
                            child: Container(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.subtitles,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Submit Task',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                        onSelected: (value) async {
                          taskName = task.data()['task_name'];
                          taskId = task.id;
                          status = task.data()['status'];
                          assignedBy = task.data()['assigned_by'];
                          assignedAt = task.data()['assigned_at'].toDate();
                          assignedTo = task.data()['assigned_to'];
                          deadline = task.data()['deadline'].toDate();
                          if (value == 1) {
                            showBottomSheet(
                              context: context,
                              builder: (context) => TaskAssign(),
                            );
                          } else if (value == 2) {
                            deleteDialog(context, task);
                          } else if (value == 3) {
                            SubmitTaskService().refreshSubmitModel();
                            Navigator.pushNamed(context, '/submit-task');
                          }
                        },
                      ),
                    ),
                ],
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    'by',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    child: AvatarLetter(
                      size: 30,
                      backgroundColor: Colors.green,
                      textColor: Colors.black,
                      fontSize: 15,
                      numberLetters: 1,
                      upperCase: true,
                      letterType: LetterType.Circular,
                      text: task['assigned_by'],
                      backgroundColorHex: '',
                      textColorHex: '',
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    task['assigned_by'],
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'to',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    child: AvatarLetter(
                      size: 30,
                      backgroundColor: Colors.amber,
                      textColor: Colors.black,
                      fontSize: 15,
                      numberLetters: 1,
                      upperCase: true,
                      letterType: LetterType.Circular,
                      text: task['assigned_to'][0],
                      backgroundColorHex: '',
                      textColorHex: '',
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    task['assigned_to'][0],
                    style: TextStyle(
                        color: Colors.grey[400],
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  _assignedList(task),
                ],
              ),
            ],
          ),
        )
      ],
    ),
  );
}

Widget _assignedList(DocumentSnapshot task) => PopupMenuButton(
      itemBuilder: (context) => [
        PopupMenuItem(
          child: Text(
            task['assigned_to']
                .toString()
                .substring(1, task['assigned_to'].toString().length - 1),
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
      ],
      color: Colors.grey[900],
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Color(0XFF879A5B),
        ),
        height: 20,
        width: 40,
        child: Center(
          child: Text(
            '+' + (task['assigned_to'].length - 1).toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
