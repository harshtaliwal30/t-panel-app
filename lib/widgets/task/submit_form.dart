import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/submit_task_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/screens/tasks/submit_task_screen.dart';
import 'package:t_panel_app/services/notification_service.dart';
import 'package:t_panel_app/services/submit_task_service.dart';
import 'package:t_panel_app/services/task_entry_service.dart';

class SubmitToggle extends StatefulWidget {
  @override
  _SubmitToggleState createState() => _SubmitToggleState();
}

class _SubmitToggleState extends State<SubmitToggle> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Mark task as Submitted',
                style: GoogleFonts.baloo(
                  textStyle: TextStyle(
                    color: Colors.amber,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomRight,
                child: Switch(
                  value: isSubmitted,
                  activeColor: Colors.blue,
                  onChanged: (value) {
                    print("VALUE : $value");
                    setState(() {
                      isSubmitted = value;
                    });
                  },
                ),
              ),
            ),
          ],
        ),
        Divider(
          color: Colors.amber,
        ),
      ],
    );
  }
}

class ReviewToggle extends StatefulWidget {
  @override
  _ReviewToggleState createState() => _ReviewToggleState();
}

class _ReviewToggleState extends State<ReviewToggle> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Mark Task as Completed',
                style: GoogleFonts.baloo(
                  textStyle: TextStyle(
                    color: Colors.amber,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomRight,
                child: Switch(
                  value: isReviewed,
                  activeColor: Colors.blue,
                  onChanged: (value) {
                    print("VALUE : $value");
                    isReviewed = value;
                    setState(() {
                      assignedBy.contains(currentUsername) ||
                              panelAdmins.contains(currentUsername)
                          ? SubmitTaskService().markIsReviewed()
                          : SubmitTaskState().checkReviwer(context,
                              'You do not have the right to mark the task as completed');
                      if (isReviewed) {
                        status = 'Completed';
                        TaskEntryService()
                            .updateTask(deadline, taskName, assignedTo);
                        NotificationService().pushNotification(
                          title: 'Work Approved',
                          msg: currentUsername +
                              ' has approved your work in ' +
                              taskName +
                              'and has marked the task as completed',
                          screen: 'notification',
                          sendTo: assignedTo,
                        );
                      } else {
                        status == 'Completed'
                            ? isSubmittedLate
                                ? status = 'Incompleted'
                                : status = 'Ongoing'
                            : status = status;
                        TaskEntryService()
                            .updateTask(deadline, taskName, assignedTo);
                      }
                    });
                  },
                ),
              ),
            ),
          ],
        ),
        Divider(
          color: Colors.amber,
        ),
      ],
    );
  }
}

class SubmitButton extends StatefulWidget {
  @override
  _SubmitButtonState createState() => _SubmitButtonState();
}

class _SubmitButtonState extends State<SubmitButton> {
  List notificationReceiver =
      [...panelAdmins, ...assignedBy.split(' ')].toSet().toList();
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        isSubmittedLate = status == 'Incompleted' ? true : false;
        if (isSubmitted == false) {
          if (formKey.currentState.validate()) {
            setState(() {
              submitDescriptionController.clear();
              isSubmitted = true;
              filepick = false;
            });

            if (file != null) {
              SubmitTaskService().uploadFile();
              NotificationService().pushNotification(
                title: 'Work Submitted',
                msg: currentUsername + ' submitted the work in ' + taskName,
                screen: 'notification',
                sendTo: notificationReceiver,
              );
            } else {
              SubmitTaskService().submitTask();
            }
          }
        } else {
          isSubmitted = false;
          print(isSubmitted);
          SubmitTaskService().markIsSubmitted();
        }
      },
      color: Colors.pink,
      elevation: 8.0,
      padding: EdgeInsets.all(10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Text(
        isSubmitted ? "Unsubmit" : "Submit",
        style: GoogleFonts.baloo(
          textStyle: TextStyle(
            color: Colors.black,
            fontSize: 15,
          ),
        ),
      ),
    );
  }
}
