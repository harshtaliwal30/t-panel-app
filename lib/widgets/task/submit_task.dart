import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:t_panel_app/models/submit_task_model.dart';
import 'package:path/path.dart' as p;
import 'package:t_panel_app/services/submit_task_service.dart';

class SubmitTaskWidget extends StatefulWidget {
  SubmitTaskWidgetState createState() => SubmitTaskWidgetState();
}

class SubmitTaskWidgetState extends State<SubmitTaskWidget> {
  Widget descriptionForm(BuildContext context) {
    submitDescriptionController.text =
        documentFields.data().containsKey('submitDescription')
            ? documentFields['submitDescription'] != null
                ? documentFields['submitDescription']
                : ''
            : '';
    return TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: null,
      enabled: !isSubmitted,
      controller: submitDescriptionController,
      style: TextStyle(
        color: Colors.white,
      ),
      validator: (value) {
        if (value.isNotEmpty) {
          submitDescription = value;
        }

        return null;
      },
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(40.0),
        border: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue[900], width: 2),
        ),
        hintText: 'Description',
        hintStyle: TextStyle(color: Colors.grey),
        alignLabelWithHint: true,
      ),
    );
  }

  Widget descriptionTile(BuildContext context) {
    return ListTile(
      title: Text(
        documentFields.data().containsKey('submitDescription')
            ? documentFields['submitDescription']
            : 'No description here',
        style: TextStyle(
          color: Colors.grey,
        ),
      ),
      tileColor: Colors.blueGrey[900],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }

  Widget submittedFile(BuildContext context) {
    return documentFields.data().containsKey('url')
        ? documentFields['url'] != null
            ? Padding(
                padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: RaisedButton(
                    color: Colors.grey[900],
                    onPressed: () {
                      setState(
                        () {
                          showDialog(
                            context: context,
                            builder: (context) => Image.network(
                              documentFields['url'].toString(),
                              fit: BoxFit.cover,
                            ),
                          );
                        },
                      );
                    },
                    child: Row(
                      children: <Widget>[
                        Container(
                          color: Colors.red,
                          width: 60,
                          height: 60,
                          child: Image.network(
                            documentFields['url'].toString(),
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                documentFields['filename'].toString(),
                                style: GoogleFonts.balooBhai(
                                  textStyle: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                documentFields['filesize'].toString() +
                                    ' MB | ' +
                                    DateFormat.yMd()
                                        .add_jm()
                                        .format(documentFields['submittedAt']
                                            .toDate())
                                        .toString(),
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 10),
                              ),
                            ],
                          ),
                        ),
                        isSubmitted == false
                            ? IconButton(
                                icon: Icon(Icons.cancel_sharp),
                                onPressed: () {
                                  SubmitTaskService().deleteFile();
                                },
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
              )
            : SizedBox()
        : SizedBox;
  }

  Widget build(BuildContext context) {
    if (file != null) {
      sizeInMb = file.lengthSync() / (1024 * 1024);
      filesize = double.parse((sizeInMb).toStringAsFixed(2));
      fileName = p.basename(file.path);

      filesize = double.parse((sizeInMb).toStringAsFixed(2));

      submittedAt = DateFormat.yMd().add_jm().format(DateTime.now());
    }
    return file != null
        ? Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: RaisedButton(
                color: Colors.grey[900],
                onPressed: () {
                  setState(
                    () {
                      showDialog(
                          context: context,
                          builder: (context) => Image.file(file));
                    },
                  );
                },
                child: Row(
                  children: <Widget>[
                    Container(
                        color: Colors.red,
                        width: 60,
                        height: 60,
                        child: Image.file(file)),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            fileName,
                            style: GoogleFonts.balooBhai(
                              textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            filesize.toString() +
                                ' MB | ' +
                                submittedAt.toString(),
                            style: TextStyle(color: Colors.grey, fontSize: 10),
                          ),
                        ],
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.cancel_sharp),
                      onPressed: () {
                        setState(
                          () {
                            file = null;
                          },
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          )
        : SizedBox();
  }
}
