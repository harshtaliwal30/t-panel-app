import 'package:flutter/material.dart';
import 'package:date_field/date_field.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/notification_service.dart';
import 'package:t_panel_app/services/task_entry_service.dart';

class TaskAssign extends StatefulWidget {
  @override
  _TaskAssignState createState() => _TaskAssignState();
}

class _TaskAssignState extends State<TaskAssign> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List _selectedNames = [];
  List<bool> toggleButtonIsSelected = [true, false, false];
  Color toggleColor = Colors.blue;
  List reqList = [];
  final taskNameController = TextEditingController();
  DateTime deadlineController;

  @override
  Widget build(BuildContext context) {
    if (taskId != null) {
      if (status == 'Ongoing')
        toggleButtonIsSelected[0] = true;
      else if (status == 'Completed') {
        toggleButtonIsSelected[0] = false;
        toggleButtonIsSelected[1] = true;
        toggleColor = Colors.green;
      } else {
        toggleButtonIsSelected[0] = false;
        toggleButtonIsSelected[2] = true;
        toggleColor = Colors.red;
      }
      taskNameController.text = taskName;
      _selectedNames = assignedTo;
      deadlineController = deadline;
      reqList = [...panelParticipants, ...assignedTo].toSet().toList();
    }
    return Container(
      height: 400,
      color: Colors.grey[900],
      child: Container(
        margin: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: new ListView(
            children: <Widget>[
              RaisedButton(
                color: Colors.green,
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    Navigator.pop(context);
                    if (taskId == null) {
                      TaskEntryService().updateLastUpdated();
                      TaskEntryService().addTask(
                        deadlineController,
                        taskNameController,
                        _selectedNames,
                      );
                      NotificationService().pushNotification(
                        title: 'New Task Assigned',
                        msg: 'Task Assigned to you in ' +
                            panelName +
                            ' by ' +
                            currentUsername,
                        screen: 'notification',
                        sendTo: _selectedNames,
                      );
                    } else {
                      TaskEntryService().updateTask(deadlineController,
                          taskNameController.text, _selectedNames);
                      NotificationService().pushNotification(
                        title: 'Task Edited',
                        msg: taskNameController.text +
                            ' in ' +
                            panelName +
                            ' has been edited by ' +
                            currentUsername,
                        screen: 'notification',
                        sendTo: _selectedNames,
                      );
                    }
                  }
                },
                child: Text(
                  'Submit',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                maxLength: 35,
                focusNode: FocusNode(),
                controller: taskNameController,
                style: TextStyle(color: Colors.white),
                onChanged: (value){
                  FocusNode().unfocus();
                },
                decoration: new InputDecoration(
                  counterStyle: TextStyle(color: Colors.white),
                  hintStyle: TextStyle(color: Colors.green),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green),
                  ),
                  labelText: 'Task Name',
                  labelStyle: TextStyle(
                    color: Colors.green,
                    fontSize: 15,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green),
                  ),
                  border: OutlineInputBorder(),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please enter Task Name";
                  } else if (value.length > 35) {
                    return "Please enter within limit";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 10,
              ),
              if (taskId != null)
                Center(
                  child: ToggleButtons(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: status == 'Ongoing' ? Colors.blue : null,
                        ),
                        height: 30,
                        margin: EdgeInsets.only(left: 10, right: 10),
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        child: Center(
                          child: Text(
                            'Ongoing',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          // border: Border.all(color: Colors.orange),
                          color: status == 'Completed' ? Colors.green : null,
                        ),
                        height: 30,
                        margin: EdgeInsets.only(left: 10, right: 10),
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        child: Center(
                          child: Text(
                            'Completed',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: status == 'Incompleted' ? Colors.red : null,
                        ),
                        height: 30,
                        margin: EdgeInsets.only(left: 10, right: 10),
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        child: Center(
                          child: Text(
                            'Incompleted',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                    constraints: BoxConstraints(minHeight: 30),
                    fillColor: Colors.grey[900],
                    isSelected: toggleButtonIsSelected,
                    onPressed: (int index) {
                      setState(() {
                        if (index == 0)
                          toggleColor = Colors.blue;
                        else if (index == 1)
                          toggleColor = Colors.green;
                        else if (index == 2) toggleColor = Colors.red;
                        for (int i = 0;
                            i < toggleButtonIsSelected.length;
                            i++) {
                          toggleButtonIsSelected[i] = i == index;
                          if (toggleButtonIsSelected[i]) {
                            if (index == 0)
                              status = 'Ongoing';
                            else if (index == 1)
                              status = 'Completed';
                            else
                              status = 'Incompleted';
                          }
                        }
                      });
                    },
                  ),
                ),
              SizedBox(
                height: 15,
              ),
              if (taskId == null ||
                  (taskId != null && (status != 'Incompleted')))
                DateTimeFormField(
                  dateTextStyle: TextStyle(color: Colors.white),
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.green),
                    errorStyle: TextStyle(color: Colors.redAccent),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.green),
                    ),
                    suffixIcon: Icon(
                      Icons.event_note,
                      color: Colors.green,
                    ),
                    labelStyle: TextStyle(color: Colors.green),
                    labelText: 'Deadline',
                  ),
                  initialValue: deadlineController,
                  mode: DateTimeFieldPickerMode.dateAndTime,
                  autovalidateMode: AutovalidateMode.always,
                  validator: (value) {
                    if (value != null) {
                      if (taskId == null) {
                        if (value.isBefore(DateTime.now()))
                          return "Date cannot be in the past";
                      } else {
                        if (status == 'Ongoing') {
                          if (value.isBefore(DateTime.now()))
                            return "Date cannot be in the past";
                        } else {
                          if (value.isAfter(DateTime.now()))
                            return "Date cannot be in the future";
                        }
                      }
                    }
                    return null;
                  },
                  onDateSelected: (DateTime value) {
                    deadlineController = value;
                    print(value);
                  },
                ),
              SizedBox(
                height: 15,
              ),
              MultiSelectDialogField(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.green,
                  ),
                ),
                searchable: true,
                backgroundColor: Colors.grey[900],
                cancelText: Text(
                  'Cancel',
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
                closeSearchIcon: Icon(Icons.close, color: Colors.green),
                searchHintStyle: TextStyle(color: Colors.green),
                confirmText: Text(
                  'Save',
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
                selectedColor: Colors.green,
                unselectedColor: Colors.grey,
                buttonIcon: Icon(Icons.arrow_downward, color: Colors.green),
                buttonText: Text(
                  'Assign',
                  style: TextStyle(color: Colors.green),
                ),
                selectedItemsTextStyle: TextStyle(color: Colors.green),
                itemsTextStyle: TextStyle(color: Colors.grey),
                searchTextStyle: TextStyle(color: Colors.green),
                searchIcon: Icon(
                  Icons.search,
                  color: Colors.green,
                ),
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: Colors.green,
                  onTap: (value) {
                    showDialog(
                      context: context,
                      builder: (ctx) {
                        return AlertDialog(
                          backgroundColor: Colors.black87,
                          title: new Text(
                            value,
                            style: TextStyle(color: Colors.pink),
                            textAlign: TextAlign.center,
                          ),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            Align(
                              alignment: Alignment.center,
                              child: new RaisedButton(
                                color: Colors.pink,
                                child: new Text(
                                  "View Profile",
                                  style: TextStyle(
                                    color: Colors.black87,
                                  ),
                                ),
                                onPressed: () {
                                 Navigator.pushNamed(context, '/work-profile');
                                },
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  textStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                title: Text(
                  'Assigned_to',
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
                initialValue: _selectedNames,
                items: assignedTo == null
                    ? panelParticipants
                        .map((e) => MultiSelectItem(e, e))
                        .toList()
                    : reqList.map((e) => MultiSelectItem(e, e)).toList(),
                listType: MultiSelectListType.LIST,
                onConfirm: (values) {
                  _selectedNames = values;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
