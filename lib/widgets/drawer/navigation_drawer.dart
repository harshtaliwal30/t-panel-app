import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:avatar_letter/avatar_letter.dart';
import 'package:google_fonts/google_fonts.dart';

class NavigationDrawer extends StatefulWidget {
  final String onPage;
  NavigationDrawer(this.onPage);

  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  String category;

  dialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Color(0xFF272727),
        content: ListTile(
          title: Icon(
            Icons.warning_rounded,
            color: Colors.grey,
            size: 40,
          ),
          subtitle: Text(
            'Alert!\nAre you sure?',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Colors.purple,
            child: Text(
              'No',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            color: Colors.purple,
            child: Text(
              'Yes',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/login', (route) => false);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xFF121212),
        child: ListView(
          children: [
            DrawerHeader(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey[800],
                          width: 3,
                        ),
                        borderRadius: BorderRadius.circular(40)),
                    child: AvatarLetter(
                      size: 60,
                      backgroundColor: Colors.amber,
                      textColor: Colors.black,
                      fontSize: 25,
                      upperCase: true,
                      numberLetters: 1,
                      letterType: LetterType.Circular,
                      text: currentUsername,
                      backgroundColorHex: '',
                      textColorHex: '',
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    currentUsername,
                    style: GoogleFonts.concertOne(
                      textStyle: TextStyle(
                        color: Colors.amber,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              height: 0,
              endIndent: 10.0,
              indent: 10.0,
              color: Colors.grey,
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/dashboard', (route) => false);
              },
              child: ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.grey,
                ),
                title: Text(
                  'Home',
                  style: GoogleFonts.baloo(
                    textStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              height: 0,
              endIndent: 30.0,
              indent: 20.0,
              color: Colors.grey,
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/account-profile',
                  arguments: {
                    'username': currentUsername,
                  },
                );
              },
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
                title: Text(
                  'Profile',
                  style: GoogleFonts.baloo(
                    textStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              height: 0,
              endIndent: 30.0,
              indent: 20.0,
              color: Colors.grey,
            ),
            if (widget.onPage != 'dashboard') ...[
              FlatButton(
                onPressed: () {
                  category = 'Completed';
                  Navigator.pushNamed(
                    context,
                    '/task-category',
                    arguments: {
                      'category': category,
                    },
                  );
                },
                child: ListTile(
                  leading: Icon(
                    Icons.timer,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Completed Tasks',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
              FlatButton(
                onPressed: () {
                  category = 'Ongoing';
                  Navigator.pushNamed(
                    context,
                    '/task-category',
                    arguments: {
                      'category': category,
                    },
                  );
                },
                child: ListTile(
                  leading: Icon(
                    Icons.timelapse,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Ongoing Tasks',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
              FlatButton(
                onPressed: () {
                  category = 'Incompleted';
                  Navigator.pushNamed(
                    context,
                    '/task-category',
                    arguments: {
                      'category': category,
                    },
                  );
                },
                child: ListTile(
                  leading: Icon(
                    Icons.timer_off_outlined,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Incompleted Tasks',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/work-profile');
                },
                child: ListTile(
                  leading: Icon(
                    Icons.receipt_long_outlined,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Work Profile',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/add-member');
                },
                child: ListTile(
                  leading: Icon(
                    Icons.person_add,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Add Member',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
            ],
            if (widget.onPage == 'dashboard') ...[
              FlatButton(
                onPressed: () {
                  panelId = null;
                  panelName = null;
                  panelDescription = null;
                  panelPurpose = null;
                  panelAdmins = null;
                  panelParticipants = null;
                  Navigator.of(context).pushNamed('/create-panel');
                },
                child: ListTile(
                  leading: Icon(
                    Icons.add,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Create Panel',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/join-panel');
                },
                child: ListTile(
                  leading: Icon(
                    Icons.add_box,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Join Panel',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
            ],
            FlatButton(
              onPressed: () {
                dialog(context);
              },
              child: ListTile(
                leading: Icon(
                  Icons.exit_to_app,
                  color: Colors.grey,
                ),
                title: Text(
                  'Logout',
                  style: GoogleFonts.baloo(
                    textStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              height: 0,
              endIndent: 30.0,
              indent: 20.0,
              color: Colors.grey,
            ),
            if (widget.onPage != 'dashboard') ...[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    '/settings',
                    arguments: {
                      'onPage': widget.onPage,
                    },
                  );
                },
                child: ListTile(
                  leading: Icon(
                    Icons.settings,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Settings',
                    style: GoogleFonts.baloo(
                      textStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(
                height: 0,
                endIndent: 30.0,
                indent: 20.0,
                color: Colors.grey,
              ),
            ]
          ],
        ),
      ),
    );
  }
}
