import 'package:avatar_letter/avatar_letter.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/add_member_service.dart';
import 'package:t_panel_app/services/notification_service.dart';

class ShowContributors extends StatefulWidget {
  final bool isAdmin;
  ShowContributors(
    this.isAdmin,
  );
  @override
  _ShowContributorsState createState() => _ShowContributorsState();
}

class _ShowContributorsState extends State<ShowContributors> {
  @override
  Widget build(BuildContext context) {
    panelCreatorDialog(context) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Color(0xFF272727),
          content: ListTile(
            title: Icon(
              Icons.person_outlined,
              color: Colors.grey,
              size: 60,
            ),
            subtitle: Text(
              "Panel Creator",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.grey, fontSize: 15),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.purple,
              child: Text(
                'Ok',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    }

    queryDialog(context, int revIndex, String operation, String msg) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Color(0xFF272727),
          content: ListTile(
            title: Icon(
              Icons.warning_rounded,
              color: Colors.grey,
              size: 40,
            ),
            subtitle: Text(
              msg,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.purple,
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              color: Colors.purple,
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () async {
                if (operation == 'contributor') {
                  await AddMemberService()
                      .deleteAdmin(panelParticipants[revIndex]);
                  panelAdmins.remove(panelParticipants[revIndex]);
                  print('Make Contributor Successful');
                  NotificationService().pushNotification(
                    title: 'T-Panel',
                    msg: currentUsername +
                        ' makes you contributor in ' +
                        panelName,
                    screen: 'notification',
                    sendTo: [panelParticipants[revIndex]],
                  );
                } else if (operation == 'admin') {
                  await AddMemberService()
                      .createAdmin(panelParticipants[revIndex]);
                  panelAdmins.add(panelParticipants[revIndex]);
                  print('Make Admin Successful');
                  NotificationService().pushNotification(
                    title: 'T-Panel',
                    msg: currentUsername + ' makes you admin in ' + panelName,
                    screen: 'notification',
                    sendTo: [panelParticipants[revIndex]],
                  );
                } else if (operation == 'remove') {
                  NotificationService().pushNotification(
                    title: 'T-Panel',
                    msg: currentUsername + ' removed you from ' + panelName,
                    screen: 'notification',
                    sendTo: [panelParticipants[revIndex]],
                  );
                  await AddMemberService()
                      .deleteAdmin(panelParticipants[revIndex]);
                  panelAdmins.remove(panelParticipants[revIndex]);
                  await AddMemberService()
                      .deleteContributor(panelParticipants[revIndex]);
                  panelParticipants.remove(panelParticipants[revIndex]);
                  print('Remove Person Successful');
                }
                setState(() {});
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        childAspectRatio: 0.81,
      ),
      itemCount: panelParticipants.length,
      itemBuilder: (context, index) {
        bool isContriAdmin = false;
        int revIndex = panelParticipants.length - index - 1;
        for (int i = 0; i < panelAdmins.length; i++) {
          if (panelAdmins[i] == panelParticipants[revIndex]) {
            isContriAdmin = true;
          }
        }
        return Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey[900],
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: Colors.grey[900],
              width: 2,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey,
                      width: 3,
                    ),
                    borderRadius: BorderRadius.circular(40)),
                child: AvatarLetter(
                  size: 45,
                  backgroundColor: Colors.green,
                  textColor: Colors.black,
                  fontSize: 20,
                  upperCase: true,
                  numberLetters: 1,
                  letterType: LetterType.Circular,
                  text: panelParticipants[revIndex],
                  backgroundColorHex: '',
                  textColorHex: '',
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                panelParticipants[revIndex],
                style: TextStyle(
                  color: Colors.purple[200],
                  fontSize: 15,
                ),
              ),
              Text(
                isContriAdmin ? 'Admin' : 'Contributor',
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 12,
                ),
              ),
              RaisedButton(
                padding: EdgeInsets.all(0),
                color: Colors.purple[400],
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    '/account-profile',
                    arguments: {
                      'username': panelParticipants[revIndex],
                    },
                  );
                },
                child: Text(
                  'View Profile',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              if (currentUsername == panelCreatorUsername ||
                  (widget.isAdmin && !isContriAdmin))
                PopupMenuButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down_sharp,
                    color: Colors.blueGrey,
                  ),
                  color: Colors.grey[900],
                  itemBuilder: (context) => [
                    !isContriAdmin
                        ? PopupMenuItem(
                            value: 1,
                            child: Container(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.admin_panel_settings,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Make Admin',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : PopupMenuItem(
                            value: 2,
                            child: Container(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.person,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Make Contributor',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                    PopupMenuItem(
                      value: 3,
                      child: Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.person_remove_rounded,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Remove',
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                  onSelected: (value) async {
                    if (value == 2) {
                      //contributor clicked
                      if (panelParticipants[revIndex] == panelCreatorUsername) {
                        panelCreatorDialog(context);
                      } else {
                        queryDialog(
                          context,
                          revIndex,
                          'contributor',
                          'Alert!\nAre you sure you want to make the person contributor.',
                        );
                      }
                    } else if (value == 1) {
                      //admin clicked
                      queryDialog(
                        context,
                        revIndex,
                        'admin',
                        'Alert!\nAre you sure you want to make the person admin.',
                      );
                    } else {
                      //remove clicked
                      if (panelParticipants[revIndex] == panelCreatorUsername) {
                        panelCreatorDialog(context);
                      } else {
                        queryDialog(
                          context,
                          revIndex,
                          'remove',
                          'Alert!\nAre you sure you want to remove the person from panel.',
                        );
                      }
                    }
                  },
                ),
            ],
          ),
        );
      },
    );
  }
}
