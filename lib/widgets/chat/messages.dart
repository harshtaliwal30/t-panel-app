import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/widgets/chat/message_bubble.dart';

class Messages extends StatelessWidget {
  final DocumentSnapshot task;
  Messages(this.task);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chats')
          .doc(task.id)
          .collection('chat')
          .orderBy('messaged_at', descending: true)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> chatSnapshot) {
        if (chatSnapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        final chatDocs = chatSnapshot.data.docs;
        return ListView.builder(
          reverse: true,
          itemCount: chatSnapshot.data.size,
          itemBuilder: (context, index) =>
              MessageBubble(chatDocs[index], task.id),
        );
      },
    );
  }
}
