import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/chat_service.dart';
import 'package:t_panel_app/services/notification_service.dart';

class NewMessage extends StatefulWidget {
  final DocumentSnapshot task;
  NewMessage(this.task);

  @override
  _NewMessageState createState() => _NewMessageState();
}

class _NewMessageState extends State<NewMessage> {
  final messageController = TextEditingController();
  var _enteredMessage = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.grey[900],
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 10,
          ),
          Icon(
            Icons.messenger_outline_rounded,
            color: Colors.grey,
            size: 18,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: TextField(
              controller: messageController,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                hintText: 'Message...',
                hintStyle: TextStyle(
                  color: Colors.grey,
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
              onChanged: (value) {
                setState(() {
                  _enteredMessage = value;
                });
              },
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.send_rounded,
              color: Colors.blue,
            ),
            onPressed: _enteredMessage.trim().isEmpty
                ? null
                : () {
                    FocusScope.of(context).unfocus();
                    messageController.clear();
                    ChatService().sendMessage(widget.task.id, _enteredMessage);
                    NotificationService().pushNotification(
                      title: 'New Message',
                      msg: 'New message from ' +
                          currentUsername +
                          ' in task ' +
                          widget.task['task_name'],
                      screen: 'notification',
                      sendTo: widget.task['assigned_to'],
                    );
                  },
          )
        ],
      ),
    );
  }
}
