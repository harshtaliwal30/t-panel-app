import 'package:avatar_letter/avatar_letter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/chat_service.dart';

class MessageBubble extends StatelessWidget {
  final DocumentSnapshot chatDoc;
  final String taskDocId;
  MessageBubble(this.chatDoc, this.taskDocId);

  @override
  Widget build(BuildContext context) {
    bool isMe = false;
    if (chatDoc['username'] == currentUsername) isMe = true;

    deleteDialog(String chatDocId, String taskDocId) {
      return showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            backgroundColor: Colors.black87,
            title: Center(
              child: Align(
                alignment: Alignment.center,
                child: new RaisedButton(
                  color: Colors.pink,
                  child: new Text(
                    "Unsend",
                    style: TextStyle(
                      color: Colors.black87,
                    ),
                  ),
                  onPressed: () {
                    ChatService().deleteMessage(taskDocId, chatDocId);
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ),
          );
        },
      );
    }

    return FlatButton(
      onLongPress: () {
        deleteDialog(chatDoc.id, taskDocId);
      },
      onPressed: () {},
      child: Row(
        mainAxisAlignment:
            isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          !isMe
              ? Container(
                  padding: EdgeInsets.all(5),
                  child: AvatarLetter(
                    size: 35,
                    backgroundColor: Colors.amber,
                    textColor: Colors.black,
                    fontSize: 15,
                    upperCase: true,
                    numberLetters: 1,
                    letterType: LetterType.Circular,
                    text: chatDoc['username'],
                    backgroundColorHex: '',
                    textColorHex: '',
                  ),
                )
              : SizedBox(),
          Container(
            decoration: BoxDecoration(
              color: isMe ? Colors.indigo[900] : Colors.grey[900],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                topRight: Radius.circular(12),
                bottomLeft: !isMe ? Radius.circular(0) : Radius.circular(12),
                bottomRight: isMe ? Radius.circular(0) : Radius.circular(12),
              ),
            ),
            width: 220,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment:
                  isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
              children: [
                Text(
                  chatDoc.data()['username'] + ':',
                  style: TextStyle(
                    color: !isMe ? Colors.grey : Colors.white,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  chatDoc.data()['text'],
                  style: TextStyle(
                    color: !isMe ? Colors.grey : Colors.white,
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
          isMe
              ? Container(
                  padding: EdgeInsets.all(5),
                  child: AvatarLetter(
                    size: 35,
                    backgroundColor: Colors.green,
                    textColor: Colors.black,
                    fontSize: 15,
                    upperCase: true,
                    numberLetters: 1,
                    letterType: LetterType.Circular,
                    text: chatDoc['username'],
                    backgroundColorHex: '',
                    textColorHex: '',
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
