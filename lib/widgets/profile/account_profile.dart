import 'package:flutter/material.dart';
import 'package:t_panel_app/models/user_model.dart';
// import 'package:t_panel_app/models/user_model.dart';
import 'package:t_panel_app/services/settings_service.dart';

class InputForm extends StatefulWidget {
  final String operation;
  InputForm(this.operation);

  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {
  var _textFieldController = TextEditingController();
  String valueText;

  @override
  Widget build(BuildContext context) {
    if (widget.operation == 'about')
      _textFieldController.text = userAbout;
    else if (widget.operation == 'profession')
      _textFieldController.text = profession;
    else if (widget.operation == 'experience')
      _textFieldController.text = experience;
    else if (widget.operation == 'fieldOfStudy')
      _textFieldController.text = fieldOfStudy;
    return TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: null,
      controller: _textFieldController,
      style: TextStyle(
        color: Colors.white,
        fontSize: 14,
      ),
      decoration: new InputDecoration(
        hintStyle: TextStyle(
          color: Colors.grey,
          fontSize: 15,
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white70),
        ),
        hintText: 'Write here...',
        suffixIcon: IconButton(
          onPressed: () async {
            if (widget.operation == 'about') {
              await SettingsService().addAbout(valueText);
              userAbout = valueText;
            } else if (widget.operation == 'profession') {
              await SettingsService().addProfession(valueText);
              profession = valueText;
            } else if (widget.operation == 'experience') {
              await SettingsService().addExperience(valueText);
              experience = valueText;
            } else if (widget.operation == 'fieldOfStudy') {
              await SettingsService().addFOS(valueText);
              fieldOfStudy = valueText;
            }
          },
          icon: Icon(Icons.cloud_upload),
          color: Colors.white70,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[800]),
        ),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Field can't be empty";
        }
        return null;
      },
      onChanged: (value) {
        valueText = value;
      },
    );
  }
}
