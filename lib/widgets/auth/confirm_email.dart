import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/routes/constant.dart';

class ConfirmEmail extends StatefulWidget {
  @override
  _ConfirmEmailState createState() => _ConfirmEmailState();
}

class _ConfirmEmailState extends State<ConfirmEmail> {
  void initState() {
    startEmailVerificationTimer();
    super.initState();
  }

  var timer;
  startEmailVerificationTimer() {
    timer = Timer.periodic(Duration(seconds: 5), (Timer _) {
      FirebaseAuth.instance.currentUser.reload();
      if (FirebaseAuth.instance.currentUser.emailVerified) {
        timer.cancel();
        Navigator.pushNamed(context, homeRoute);
        
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushNamed(context, '/login');
            }),
        title: Text("Confirm Email"),
        backgroundColor: Color(0xFF272727),
      ),
      body: Align(
        alignment: AlignmentDirectional.center,
        child: Container(
          height: 250,
          decoration: BoxDecoration(
            color: Color(0xFF272727),
            border: Border.all(
              color: Colors.white54,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  'An email has just been sent to your registered email Id. If not received click on button below.',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
              RaisedButton(
                color: Colors.green,
                onPressed: () {
                  FirebaseAuth.instance.currentUser
                      .sendEmailVerification()
                      .then(
                        (value) => ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: Colors.green,
                            content: const Text(
                              'Email has been sent',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      );
                },
                child: Text(
                  'Resend Email',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
