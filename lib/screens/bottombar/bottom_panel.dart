import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/tasks/task_entry_screen.dart';
import 'package:t_panel_app/widgets/drawer/navigation_drawer.dart';

class BottomPanel extends StatefulWidget {
  @override
  _BottomPanelState createState() => _BottomPanelState();
}

int selectedIndex = 0;

class _BottomPanelState extends State<BottomPanel> {
  Widget build(BuildContext context) {
    String onPage = 'panel';

    List<Widget> listWidgets = [
      TaskEntry(),
    ];
    return Scaffold(
      backgroundColor: Color(0XFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        backgroundColor: Color(0xFF272727),
        title: Text(panelName),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.person_add,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('/add-member');
            },
          )
        ],
      ),
      body: listWidgets[selectedIndex],
      bottomNavigationBar: ConvexAppBar(
        style: TabStyle.reactCircle,
        backgroundColor: Colors.black12,
        items: [
          TabItem(
            icon: Icon(
              Icons.today_outlined,
              color: Colors.deepOrange,
            ),
            title: 'TaskEntry',
          ),
        ],
        onTap: onItemTapped,
      ),
      drawer: NavigationDrawer(onPage),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}
