import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/widgets/chat/messages.dart';
import 'package:t_panel_app/widgets/chat/new_message.dart';

class ChatScreen extends StatelessWidget {
  final data;
  ChatScreen(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text(data['documentSnap']['task_name']),
        backgroundColor: Color(0xFF272727),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Messages(data['documentSnap']),
            ),
            NewMessage(data['documentSnap']),
          ],
        ),
      ),
    );
  }
}
