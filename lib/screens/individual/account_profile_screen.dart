import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:t_panel_app/models/submit_task_model.dart';
import 'package:t_panel_app/models/user_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/settings_service.dart';
import 'package:t_panel_app/services/submit_task_service.dart';
import 'package:t_panel_app/widgets/profile/account_profile.dart';
import 'package:t_panel_app/widgets/profile/account_profile_tile.dart';
import 'package:url_launcher/url_launcher.dart';

class AccountProfileScreen extends StatefulWidget {
  final data;
  AccountProfileScreen(this.data);

  @override
  _AccountProfileScreenState createState() => _AccountProfileScreenState();
}

class _AccountProfileScreenState extends State<AccountProfileScreen> {
  bool isLoading = false;

  addUserDetailsDialog(BuildContext context, String operation) {
    final _textFieldController = TextEditingController();
    if (operation == 'gmail')
      _textFieldController.text = email;
    else if (operation == 'github')
      _textFieldController.text = github;
    else if (operation == 'linkedin') _textFieldController.text = linkedin;
    String valueText;
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Color(0xFF272727),
          content: TextFormField(
            controller: _textFieldController,
            style: TextStyle(color: Colors.white),
            decoration: new InputDecoration(
              hintStyle: TextStyle(color: Colors.amber),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.amber),
              ),
              hintText: 'Paste Link here',
              suffixIcon: Icon(
                Icons.link,
                color: Colors.amber,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.amber),
              ),
              border: OutlineInputBorder(),
            ),
            validator: (value) {
              if (value.isEmpty) {
                return "Please enter Bio";
              }
              return null;
            },
            onChanged: (value) {
              valueText = value;
            },
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              color: Colors.amber,
              textColor: Colors.black,
              child: Text('Save'),
              onPressed: () async {
                if (operation == 'gmail') {
                  await SettingsService().addAbout(valueText);
                  email = valueText;
                } else if (operation == 'github') {
                  await SettingsService().addGithub(valueText);
                  github = valueText;
                } else if (operation == 'linkedin') {
                  await SettingsService().addLinkedin(valueText);
                  linkedin = valueText;
                }
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    SettingsService().getUserDetails(widget.data['username']).then((_) {
      setState(() {
        isLoading = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0XFF121212),
      appBar: AppBar(
        elevation: 0,
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text("Profile"),
        backgroundColor: Color(0XFF272727),
      ),
      body: !isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    height: size.height * 0.25,
                    decoration: BoxDecoration(
                      color: Colors.grey[900],
                      border: Border.all(color: Colors.grey[850]),
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Center(
                        child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('users')
                          .where('username', isEqualTo: widget.data['username'])
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return ListView(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: snapshot.data.docs
                              .map((DocumentSnapshot document) {
                            return Container(
                              height: size.height * 0.23,
                              width: size.width * 0.4,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.white70),
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: NetworkImage(document
                                              .data()
                                              .containsKey('profile_img_url') &&
                                          document['profile_img_url'].length !=
                                              0
                                      ? document['profile_img_url']
                                      : dummyProfileUrl),
                                  fit: BoxFit.contain,
                                ),
                              ),
                              child: currentUsername == widget.data['username']
                                  ? Align(
                                      alignment: AlignmentDirectional.bottomEnd,
                                      child: IconButton(
                                        onPressed: () async {
                                          profile = await FilePicker.getFile(
                                              type: FileType.image);
                                          if (profile != null) {
                                            fileType = 'profileImg';
                                            fileName =
                                                currentUsername + '_profileImg';
                                            await SubmitTaskService()
                                                .uploadFile();
                                            setState(() {});
                                          }
                                        },
                                        icon: Icon(
                                          FontAwesomeIcons.camera,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            );
                          }).toList(),
                        );
                      },
                    )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 60),
                    width: double.infinity,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Colors.purple,
                    ),
                    child: Center(
                      child: Text(
                        widget.data['username'],
                        style: GoogleFonts.baloo(
                          textStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'What about you?',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              if (currentUsername == widget.data['username'])
                                InputForm('about')
                              else
                                InputFormTile('about'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          height: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'What you are doing currently?',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              if (currentUsername == widget.data['username'])
                                InputForm('profession')
                              else
                                InputFormTile('profession'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          height: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Field of study:',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              if (currentUsername == widget.data['username'])
                                InputForm('fieldOfStudy')
                              else
                                InputFormTile('fieldOfStudy'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          height: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Work Experience:',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              if (currentUsername == widget.data['username'])
                                InputForm('experience')
                              else
                                InputFormTile('experience'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          height: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Reach me out at:',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                color: Colors.blueGrey[900],
                                child: Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () async {
                                              final Uri params = Uri(
                                                scheme: 'mailto',
                                                path: email,
                                                query:
                                                    'subject=&body=', //add subject and body here
                                              );

                                              var url = params.toString();
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                throw 'Could not launch $url';
                                              }
                                            },
                                            child: Image.asset(
                                              'assets/images/gmail-logo.png',
                                              color: email != null
                                                  ? null
                                                  : Colors.grey,
                                              colorBlendMode: BlendMode.color,
                                              fit: BoxFit.fill,
                                              width: 35,
                                              height: 25,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          if (false)
                                            RaisedButton(
                                              onPressed: () {
                                                addUserDetailsDialog(
                                                    context, 'gmail');
                                              },
                                              color: Colors.green,
                                              child: Text("Add link"),
                                            ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              if (github.isNotEmpty) {
                                                launch(github);
                                              }
                                            },
                                            child: Image.asset(
                                              'assets/images/github-logo.png',
                                              color: github != null
                                                  ? null
                                                  : Colors.grey,
                                              colorBlendMode: BlendMode.color,
                                              fit: BoxFit.fill,
                                              width: 40,
                                              height: 35,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          if (currentUsername ==
                                              widget.data['username'])
                                            RaisedButton(
                                              onPressed: () {
                                                addUserDetailsDialog(
                                                    context, 'github');
                                              },
                                              color: Colors.green,
                                              child: Text("Add link"),
                                            ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              if (linkedin.isNotEmpty) {
                                                launch(linkedin);
                                              }
                                            },
                                            child: Image.asset(
                                              'assets/images/linkedin-logo.png',
                                              color: linkedin != null
                                                  ? null
                                                  : Colors.grey,
                                              colorBlendMode: BlendMode.color,
                                              fit: BoxFit.fill,
                                              width: 40,
                                              height: 40,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          if (currentUsername ==
                                              widget.data['username'])
                                            RaisedButton(
                                              onPressed: () {
                                                addUserDetailsDialog(
                                                    context, 'linkedin');
                                              },
                                              color: Colors.green,
                                              child: Text("Add link"),
                                            ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
