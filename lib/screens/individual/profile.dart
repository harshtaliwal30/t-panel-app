import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/screens/individual/progress_chart.dart';
import 'package:t_panel_app/services/task_entry_service.dart';
import 'package:t_panel_app/widgets/task/all_tasks.dart';

//import 'line_chart/line_chart_page.dart';
class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int selectedIndex;
  var status;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF121212),
      body: DefaultTabController(
        length: 3,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backwardsCompatibility: false,
                systemOverlayStyle: SystemUiOverlayStyle(
                  statusBarIconBrightness: Brightness.light,
                ),
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                backgroundColor: Color(0xFF272727),
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(
                    currentUsername.toUpperCase(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  background: ProgressChart(),
                ),
              ),
              SliverPersistentHeader(
                delegate: _SliverAppBarDelegate(
                  TabBar(
                    labelColor: Colors.pink[300],
                    unselectedLabelColor: Colors.deepOrange,
                    onTap: onItemTapped,
                    tabs: [
                      Tab(
                        icon: Icon(Icons.timelapse),
                        text: "Ongoing",
                      ),
                      Tab(
                        icon: Icon(Icons.timer_sharp),
                        text: "Completed",
                      ),
                      Tab(
                        icon: Icon(Icons.timer_off_sharp),
                        text: "Incompleted",
                      ),
                    ],
                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: StreamBuilder<QuerySnapshot>(
            stream: TaskEntryService().queryProfile(selectedIndex),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return Text('Something went wrong');
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              return Container(
                color: Color(0XFF121212),
                child: new ListView(
                  children: snapshot.data.docs.map((DocumentSnapshot tasks) {
                    return showTasks(context, tasks);
                  }).toList(),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
