import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/dashboard_service.dart';
import 'package:t_panel_app/widgets/drawer/navigation_drawer.dart';
import 'package:t_panel_app/widgets/dashboard/no_panel.dart';
import 'package:t_panel_app/widgets/dashboard/panels.dart';
import 'package:t_panel_app/services/notification_service.dart';
import 'package:t_panel_app/services/shared_prefs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  MyLocalStorage _storage = MyLocalStorage();
  CollectionReference panels = FirebaseFirestore.instance.collection('panel');
  String onPage = 'dashboard';
  bool isLoading = false;

  @override
  void initState() {
    DashboardService().getUsername().then((value) {
      setState(() {
        isLoading = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      _storage.getDeviceToken().then((value) {
        if (value == null) {
          _messaging.getToken().then((fcmToken) {
            print("fcm saved to storage!");
            NotificationService().postDeviceToken(fcmToken: fcmToken);
            _storage.setDeviceToken(fcmToken);
          });
        }
      });
    }
    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        title: Text("T-Panel"),
        backgroundColor: Color(0xFF272727),
      ),
      body: !isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: panels
                        .where('participants', arrayContains: currentUsername)
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.data.size == 0) {
                        return NoPanel();
                      } else {
                        return new ListView(
                          children: snapshot.data.docs
                              .map((DocumentSnapshot document) {
                            return Panels(document);
                          }).toList(),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
      drawer: NavigationDrawer(onPage),
    );
  }
}
