import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/submit_task_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/task_entry_service.dart';
import 'package:t_panel_app/widgets/task/submit_form.dart';
import 'package:t_panel_app/widgets/task/submit_task.dart';

class SubmitTask extends StatefulWidget {
  @override
  SubmitTaskState createState() => SubmitTaskState();
}

class SubmitTaskState extends State<SubmitTask> {
  checkReviwer(context, dialogMsg) {
    isReviewed = false;
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.grey[900],
        title: Text(dialogMsg, style: TextStyle(color: Colors.white60)),
      ),
    );
  }

  fileLimitDialog(context) {
    isReviewed = false;
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.grey[900],
        title: Text(
            'You cannot upload image with size greater than ' +
                fileLimit.toString() +
                ' MB',
            style: TextStyle(color: Colors.white60)),
      ),
    );
  }

  Future filePicker(BuildContext context) async {
    try {
      file = await FilePicker.getFile(type: FileType.any);
      sizeInMb = file != null ? file.lengthSync() / (1024 * 1024) : 0;
      filesize = file != null ? double.parse((sizeInMb).toStringAsFixed(2)) : 0;
      fileName = p.basename(file.path);
      fileExtension = p.extension(file.path);
      if (extensionArray[0].contains(fileExtension)) {
        fileType = 'image';
        fileLimit = 6;
        if (filesize > fileLimit) {
          setState(() {
            fileLimitDialog(context);
            file = null;
          });
        }
      } else if (extensionArray[1].contains(fileExtension)) {
        fileType = 'video';
        fileLimit = 5;
        if (filesize > fileLimit) {
          setState(() {
            fileLimitDialog(context);
          });
        }
      } else if (extensionArray[2].contains(fileExtension)) {
        fileType = 'pdf';
        fileLimit = 5;
        if (filesize > fileLimit) {
          setState(() {
            fileLimitDialog(context);
          });
        }
      } else {
        fileType = 'others';
        fileLimit = 2;
        if (filesize > fileLimit) {
          setState(() {
            fileLimitDialog(context);
          });
        }
      }
      setState(
        () {
          if (file != null) {
            fileName = p.basename(file.path);
            filepick = true;
          } else {
            filepick = false;
          }
        },
      );
    } on PlatformException catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Sorry...'),
              content: Text('Unsupported exception: $e'),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        backgroundColor: Color(0XFF272727),
        title: Text('Submit Task'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        children: [
          StreamBuilder<DocumentSnapshot>(
            stream: firestoreInstance
                .collection('panel')
                .doc(panelId)
                .collection('task')
                .doc(taskId)
                .snapshots(),
            builder: (BuildContext context,
                AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.hasError) {
                return Text('Something went wrong');
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.hasData) {
                documentFields = snapshot.data;
                isSubmitted = documentFields.data().containsKey('isSubmitted')
                    ? documentFields['isSubmitted']
                    : false;
                isReviewed = documentFields.data().containsKey('isReviewed')
                    ? documentFields['isReviewed']
                    : false;
                isSubmittedLate =
                    documentFields.data().containsKey('isSubmittedLate')
                        ? documentFields['isSubmittedLate']
                        : status == 'Incompleted'
                            ? true
                            : false;
                return Form(
                  key: formKey,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                isSubmitted
                                    ? 'Submitted Files'
                                    : 'Uploaded Files',
                                style: GoogleFonts.baloo(
                                  textStyle: TextStyle(
                                    color: Colors.amber,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            documentFields.data().containsKey('filename')
                                ? documentFields['filename'] != null
                                    ? SubmitTaskWidgetState()
                                        .submittedFile(context)
                                    : Text(
                                        isSubmitted
                                            ? 'No submitted files'
                                            : 'No Uploaded Files',
                                        style: TextStyle(color: Colors.grey),
                                      )
                                : Text(
                                    isSubmitted
                                        ? 'No submitted files'
                                        : 'No Uploaded Files',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                            SizedBox(height: 10),
                            Divider(
                              color: Colors.amber,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        assignedTo.contains(currentUsername)
                            ? Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      'Upload Attachment',
                                      style: GoogleFonts.baloo(
                                        textStyle: TextStyle(
                                          color: Colors.amber,
                                          fontSize: 15,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    width: 700,
                                    child: RaisedButton(
                                      onPressed: () {
                                        setState(() {
                                          fileType = 'others';
                                        });
                                        isSubmitted
                                            ? checkReviwer(context,
                                                'Before doing changes Unsubmit the task')
                                            : filePicker(context);
                                      },
                                      color: Colors.blue[900],
                                      elevation: 8.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8),
                                        side: BorderSide.none,
                                      ),
                                      child: Text(
                                        'Attachment',
                                        style: GoogleFonts.balooBhai(
                                          textStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  filepick
                                      ? SubmitTaskWidget()
                                      : SizedBox(height: 0),
                                  Divider(
                                    color: Colors.amber,
                                  ),
                                ],
                              )
                            : SizedBox(),
                        ReviewToggle(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Description',
                            style: GoogleFonts.baloo(
                              textStyle: TextStyle(
                                color: Colors.amber,
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        assignedTo.contains(currentUsername)
                            ? SubmitTaskWidgetState().descriptionForm(context)
                            : SubmitTaskWidgetState().descriptionTile(context),
                        SizedBox(height: 20),
                        assignedTo.contains(currentUsername)
                            ? SubmitButton()
                            : SizedBox()
                      ],
                    ),
                  ),
                );
              }
              return SizedBox();
            },
          ),
        ],
      ),
    );
  }
}
