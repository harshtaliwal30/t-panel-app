import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/services/task_entry_service.dart';
import 'package:t_panel_app/widgets/task/task_assign.dart';
import 'package:t_panel_app/widgets/task/all_tasks.dart';

class TaskEntry extends StatefulWidget {
  @override
  _InputTask createState() => new _InputTask();
}

class _InputTask extends State<TaskEntry> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniStartFloat,
      backgroundColor: Color(0xFF121212),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: firestoreInstance
                  .collection('panel')
                  .doc(panelId)
                  .collection('task')
                  .snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text('Something went wrong');
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                // if (snapshot.data.size == 0) {
                //   return Column(
                //     mainAxisAlignment: MainAxisAlignment.end,
                //     children: [
                //       Align(
                //         alignment: Alignment.bottomCenter,
                //         child: Image.asset(
                //           'assets/images/arrow.gif',
                //           height: 200,
                //           fit: BoxFit.cover,
                //         ),
                //       ),
                //       SizedBox(
                //         height: 50,
                //       )
                //     ],
                //   );
                // } else {
                return Container(
                  margin: EdgeInsets.all(5),
                  child: new ListView(
                    children: snapshot.data.docs.map((DocumentSnapshot tasks) {
                      return showTasks(context, tasks);
                    }).toList(),
                  ),
                );
                // }
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.amber,
        label: Text(
          'Assign Task',
          style: TextStyle(
            color: Colors.grey[800],
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        icon: Icon(
          Icons.add,
          color: Colors.grey[800],
        ),
        onPressed: () async {
          taskName = null;
          taskId = null;
          status = null;
          assignedBy = null;
          assignedAt = null;
          assignedTo = null;
          deadline = null;
          showBottomSheet(
            context: context,
            builder: (context) => TaskAssign(),
          );
        },
      ),
    );
  }
}
