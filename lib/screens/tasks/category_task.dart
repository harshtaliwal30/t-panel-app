import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/widgets/task/all_tasks.dart';

class CategoryTask extends StatefulWidget {
  final data;
  CategoryTask(this.data);
  CategoryTaskState createState() => CategoryTaskState();
}

class CategoryTaskState extends State<CategoryTask> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        backgroundColor: Color(0XFF272727),
        title: Text(widget.data['category'] + 'Tasks'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      backgroundColor: Color(0XFF121212),
      body: StreamBuilder<QuerySnapshot>(
        stream: taskCategory(widget.data['category'], panelId),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          print(snapshot.data.size);
          return Container(
            margin: EdgeInsets.all(5),
            child: new ListView(
              children: snapshot.data.docs.map((DocumentSnapshot tasks) {
                return showTasks(context, tasks);
              }).toList(),
            ),
          );
        },
      ),
    );
  }
}
