import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:random_string/random_string.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/create_panel_service.dart';

class CreatePanel extends StatefulWidget {
  @override
  _CreatePanelState createState() => _CreatePanelState();
}

class _CreatePanelState extends State<CreatePanel> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();

  String _panelName = '';
  String _purpose = '';
  String _description = '';

  final panelNameController = TextEditingController();
  final purposeController = TextEditingController();
  final descriptionController = TextEditingController();

  dialog(BuildContext context, message) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Color(0xFF272727),
        content: ListTile(
          title: Icon(
            Icons.cloud_done_sharp,
            color: Colors.grey,
            size: 60,
          ),
          subtitle: Text(
            message,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey, fontSize: 15),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Colors.purple,
            child: Text(
              'Ok',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () => Navigator.of(context)
                .pushNamedAndRemoveUntil('/dashboard', (route) => false),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (panelDescription != null && panelName != null && panelPurpose != null) {
      panelNameController.text = panelName;
      purposeController.text = panelPurpose;
      descriptionController.text = panelDescription;
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text("Create T-Panel"),
        backgroundColor: Color(0xFF272727),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/image4.png',
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 2,
                      ),
                    ),
                    counterStyle: TextStyle(color: Colors.white),
                    labelText: "Panel Name",
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  maxLength: 25,
                  controller: panelNameController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter Panel Name";
                    } else if (value.length > 25) {
                      return "Please enter within limit";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _panelName = value;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 2,
                      ),
                    ),
                    counterStyle: TextStyle(color: Colors.white),
                    labelText: "Purpose",
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  maxLength: 40,
                  controller: purposeController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter purpose of panel";
                    } else if (value.length > 40) {
                      return "Please enter within limit";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _purpose = value;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 2,
                      ),
                    ),
                    labelText: "Description",
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  controller: descriptionController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter description";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _description = value;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                width: double.infinity,
                child: FlatButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      if (panelDescription == null &&
                          panelName == null &&
                          panelPurpose == null) {
                        CreatePanelService().postPanel(
                          panelName: _panelName,
                          description: _description,
                          purpose: _purpose,
                          admins: FieldValue.arrayUnion([currentUsername]),
                          panelCreator: currentUsername,
                          participants:
                              FieldValue.arrayUnion([currentUsername]),
                          panelId: randomAlphaNumeric(7),
                        );
                        print('Panel Created');
                        dialog(context, "Panel Created");
                      } else {
                        CreatePanelService().updatePanel(
                            panelName: _panelName,
                            description: _description,
                            purpose: _purpose,
                            panelId: panelId);
                        print('Panel Updated');
                        dialog(context, "Panel Updated");
                      }
                      panelNameController.clear();
                      purposeController.clear();
                      descriptionController.clear();
                    }
                  },
                  color: Colors.purple,
                  child: Text(
                    panelDescription == null &&
                            panelName == null &&
                            panelPurpose == null
                        ? "Create"
                        : "Update",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
