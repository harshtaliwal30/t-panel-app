import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/services/join_panel_service.dart';

class JoinPanel extends StatelessWidget {
  final panelIdController = TextEditingController();
  final firestoreInstance = FirebaseFirestore.instance;

  dialog(BuildContext context, message) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Color(0xFF272727),
        content: ListTile(
          title: Icon(
            Icons.cloud_done_sharp,
            color: Colors.grey,
            size: 60,
          ),
          subtitle: Text(
            message,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey, fontSize: 15),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Colors.purple,
            child: Text(
              'Ok',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () => Navigator.of(context)
                .pushNamedAndRemoveUntil('/dashboard', (route) => false),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool flag = false;

    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text("Join T-Panel"),
        backgroundColor: Color(0xFF272727),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                'assets/images/image4.png',
                height: 200,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: TextField(
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey,
                      width: 2,
                    ),
                  ),
                  labelText: "Enter Panel Code",
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                style: TextStyle(
                  color: Colors.white,
                ),
                controller: panelIdController,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
              width: double.infinity,
              child: RaisedButton(
                onPressed: () async {
                  await JoinPanelService()
                      .joinPanel(context, panelIdController)
                      .then((value) {
                    flag = value;
                  });
                  if (flag)
                    dialog(context, 'Panel Joined!');
                  else
                    dialog(context, 'Panel Not Found');
                },
                color: Colors.blue,
                child: Text(
                  "Join",
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
