import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/models/user_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/widgets/auth/auth_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _auth = FirebaseAuth.instance;

  void _submitAuthScreen(
    String email,
    String username,
    String password,
    isLogin,
    BuildContext ctx,
  ) async {
    if (isLogin) {
      setState(() {
        isLoading = 'Ongoing';
      });
    }

    try {
      if (isLogin) {
        authResult = await _auth.signInWithEmailAndPassword(
          email: email,
          password: password,
        );
        await FirebaseFirestore.instance
            .collection('users')
            .doc(authResult.user.uid)
            .get()
            .then((value) {
          currentUsername = value.data()['username'];
          setState(() {
            isLoading = 'Completed';
          });
          print(currentUsername);
        });
      } else {
        authResult = await _auth.createUserWithEmailAndPassword(
          email: email,
          password: password,
        );
        await FirebaseFirestore.instance
            .collection('users')
            .doc(authResult.user.uid)
            .set(
          {
            'username': username,
            'email': email,
            'about': '',
          },
        );
        try {
          await authResult.user.sendEmailVerification();
        } catch (e) {
          print(
              "An error occured while trying to send email        verification");
          print(e.message);
        }

        setState(() {
          isLoading = 'Completed';
        });
        currentUsername = username;
      }
    } on PlatformException catch (err) {
      var message = 'An error occurred, please check your credentials!';
      print(1);
      print(message);
      if (err.message != null) {
        message = err.message;
        print(2);
        print(message);
      }
      ScaffoldMessenger.of(ctx).showSnackBar(
        SnackBar(
          content: Text(message),
          backgroundColor: Colors.grey[800],
        ),
      );
      print(message);
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        title: Text("Authentication"),
        backgroundColor: Color(0xFF272727),
      ),
      body: AuthForm(_submitAuthScreen),
    );
  }
}
