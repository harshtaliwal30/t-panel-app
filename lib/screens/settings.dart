import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/settings_service.dart';

class Settings extends StatefulWidget {
  final data;
  Settings(this.data);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  deleteDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Color(0xFF272727),
        content: ListTile(
          title: Text(
            'Are you sure?',
            style: TextStyle(
              color: Colors.grey,
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Colors.purple,
            child: Text(
              'No',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          FlatButton(
            color: Colors.purple,
            child: Text(
              'Yes',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () async {
              if (currentUsername == panelCreatorUsername)
                await SettingsService().deletePanel();
              else {
                await SettingsService().deleteAdmin();
                await SettingsService().deleteContributor();
              }
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/dashboard', (route) => false);
            },
          ),
        ],
      ),
    );
  }

  panelCodeDialog(BuildContext context, String panelCode) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Color(0xFF272727),
        content: ListTile(
          title: Icon(
            Icons.visibility,
            color: Colors.grey,
            size: 60,
          ),
          subtitle: Text(
            panelCode,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey, fontSize: 20),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Colors.purple,
            child: Text(
              'Close',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
        title: Text("Settings"),
        backgroundColor: Color(0xFF272727),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          // --------------------------------Panel Settings start-------------------------------------->
          if (widget.data['onPage'] != 'dashboard') ...[
            Row(
              children: [
                SizedBox(
                  width: 47,
                ),
                Text(
                  'Panel Settings',
                  style: TextStyle(color: Colors.cyan),
                ),
              ],
            ),
            if (currentUsername != panelCreatorUsername)
              Container(
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(
                            Icons.delete,
                            color: Colors.amber,
                          ),
                        ),
                        Text(
                          'Exit Panel',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    FlatButton(
                      onPressed: () {
                        deleteDialog(context);
                      },
                      child: Text(
                        'Exit',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      color: Colors.red,
                    )
                  ],
                ),
              )
            else
              Container(
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(
                            Icons.delete,
                            color: Colors.amber,
                          ),
                        ),
                        Text(
                          'Delete Panel',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    FlatButton(
                      onPressed: () {
                        deleteDialog(context);
                      },
                      child: Text(
                        'Delete',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      color: Colors.red,
                    )
                  ],
                ),
              ),
            Container(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.visibility_off_outlined,
                          color: Colors.amber,
                        ),
                      ),
                      Text(
                        'Panel Code',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  FlatButton(
                    onPressed: () {
                      panelCodeDialog(context, panelCode);
                    },
                    child: Text(
                      'Show',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    color: Colors.green,
                  ),
                ],
              ),
            ),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pushNamed('/create-panel');
              },
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.edit,
                      color: Colors.amber,
                    ),
                  ),
                  Text(
                    'Edit Panel Detailes',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 5,
              color: Colors.grey[900],
            ),
          ],
          // --------------------------------Panel Settings end-------------------------------------->
        ],
      ),
    );
  }
}
