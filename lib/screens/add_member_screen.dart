import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/add_member_service.dart';
import 'package:t_panel_app/services/notification_service.dart';
import '../widgets/drawer/contributors.dart';

class AddMember extends StatefulWidget {
  @override
  _AddMemberState createState() => _AddMemberState();
}

class _AddMemberState extends State<AddMember> {
  List allUsers = [];
  List _selectedNames = [];
  bool isLoading = true;
  bool isAdmin = false;

  @override
  void initState() {
    AddMemberService().getUsers().then((value) {
      value.forEach((element) {
        allUsers.add(element);
      });
      setState(() {
        isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List contributors = panelParticipants;
    List admins = panelAdmins;
    List reqUsers = [];
    allUsers.forEach((element) {
      if (!contributors.contains(element)) {
        reqUsers.add(element);
      }
    });
    for (int i = 0; i < admins.length; i++) {
      if (admins[i] == currentUsername) {
        isAdmin = true;
      }
    }
    return Scaffold(
      backgroundColor: Color(0xFF121212),
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text("Contributor"),
        backgroundColor: Color(0xFF272727),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  isAdmin
                      ? Container(
                          child: MultiSelectBottomSheetField(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.purple),
                            ),
                            backgroundColor: Color(0xFF272727),
                            initialChildSize: 0.7,
                            maxChildSize: 0.95,
                            title: Text(
                              "Members",
                              style: TextStyle(
                                color: Colors.purple,
                              ),
                            ),
                            buttonText: Text(
                              "Add Members",
                              style: TextStyle(
                                color: Colors.purple,
                              ),
                            ),
                            cancelText: Text(
                              'Cancel',
                              style: TextStyle(
                                color: Colors.purple,
                              ),
                            ),
                            closeSearchIcon: Icon(
                              Icons.close,
                              color: Colors.purple,
                            ),
                            confirmText: Text(
                              'Save',
                              style: TextStyle(
                                color: Colors.purple,
                              ),
                            ),
                            selectedColor: Colors.purple,
                            unselectedColor: Colors.grey,
                            buttonIcon: Icon(
                              Icons.arrow_downward,
                              color: Colors.purple,
                            ),
                            selectedItemsTextStyle: TextStyle(
                              color: Colors.purple,
                            ),
                            itemsTextStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            searchTextStyle: TextStyle(
                              color: Colors.purple,
                            ),
                            searchIcon: Icon(
                              Icons.search,
                              color: Colors.purple,
                            ),
                            items: reqUsers
                                .map((e) => MultiSelectItem(e, e))
                                .toList(),
                            searchable: true,
                            validator: (values) {
                              if (values == null || values.isEmpty) {
                                return "Required";
                              }
                              return null;
                            },
                            onConfirm: (values) {
                              setState(() {
                                _selectedNames = values;
                              });
                            },
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.lock_outline_rounded,
                              color: Colors.white70,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Admin Section',
                              style: TextStyle(
                                color: Colors.white70,
                              ),
                            )
                          ],
                        ),
                  SizedBox(
                    height: 10,
                  ),
                  isAdmin
                      ? RaisedButton(
                          color: Colors.purple,
                          onPressed: () async {
                            if (_selectedNames.length != 0) {
                              await AddMemberService()
                                  .createContributor(_selectedNames);
                              panelParticipants.addAll(_selectedNames);
                              print('Member added successfully.');
                              NotificationService().pushNotification(
                                title: 'T-Panel',
                                msg: currentUsername +
                                    ' added you in ' +
                                    panelName,
                                screen: 'notification',
                                sendTo: _selectedNames,
                              );
                              setState(() {
                                _selectedNames.clear();
                              });
                            }
                          },
                          child: Text(
                            "Add",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        )
                      : SizedBox(),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  Expanded(
                    child: ShowContributors(isAdmin),
                  ),
                ],
              ),
            ),
    );
  }
}
