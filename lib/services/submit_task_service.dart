import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/submit_task_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/models/user_model.dart';
import 'package:t_panel_app/services/settings_service.dart';

class SubmitTaskService extends StatelessWidget {
  Future<void> uploadFile() async {
    storageReference =
        FirebaseStorage.instance.ref().child("$fileType/$fileName");

    uploadTask = storageReference.putFile(file != null ? file : profile);

    uploadTask.snapshotEvents.listen(
      (TaskSnapshot snapshot) async {
        print('Snapshot state: ${snapshot.state}'); // paused, running, complete
        print('Progress: ${snapshot.totalBytes / snapshot.bytesTransferred}');
      },
      onError: (Object e) {
        print(e); // FirebaseException
      },
    );
    uploadTask.then((TaskSnapshot snapshot) async {
      fileType == 'profileImg'
          ? profileUrl = await snapshot.ref.getDownloadURL()
          : downloadUrl = await snapshot.ref.getDownloadURL();
      if (downloadUrl != null) {
        await submitTask();
      } else if (profileUrl != null) {
        await SettingsService().addProfileImgUrl();
      }
      print('Upload complete!');
    }).catchError(
      (Object e) {
        print(e); // FirebaseException
      },
    );
  }

  Future<void> deleteFile() async {
    storageReference =
        FirebaseStorage.instance.refFromURL(documentFields['url']);

    storageReference
        .delete()
        .then(
          (value) => {print('file deleted'), fileUnsubmitted()},
        )
        .catchError(
      (Object e) {
        print(e); // FirebaseException
      },
    );
  }

  Future<void> submitTask() async {
    await FirebaseFirestore.instance
        .collection("panel")
        .doc(panelId)
        .collection('task')
        .doc(taskId)
        .update(
      {
        "url": downloadUrl != null && downloadUrl != ''
            ? downloadUrl
            : documentFields.data().containsKey('url')
                ? documentFields['url']
                : null,
        "filename": fileName != null && fileName != ''
            ? fileName
            : documentFields.data().containsKey('filename')
                ? documentFields['filename']
                : null,
        "filesize": filesize != null && filesize != 0
            ? filesize
            : documentFields.data().containsKey('filesize')
                ? documentFields['filesize']
                : null,
        "isSubmitted": isSubmitted,
        "isReviewed": isReviewed,
        "submitDescription": submitDescription,
        "submittedAt": DateTime.now(),
        "isSubmittedLate": isSubmittedLate
      },
    );
    print("URL is ");
  }

  Future<void> markIsSubmitted() async {
    await FirebaseFirestore.instance
        .collection("panel")
        .doc(panelId)
        .collection('task')
        .doc(taskId)
        .update(
      {
        "isSubmitted": isSubmitted,
      },
    );
    print("URL is ");
  }

  Future<void> markIsReviewed() async {
    await FirebaseFirestore.instance
        .collection("panel")
        .doc(panelId)
        .collection('task')
        .doc(taskId)
        .update(
      {
        "isReviewed": isReviewed,
      },
    );
    print("URL is ");
  }

  Future<void> fileUnsubmitted() async {
    await FirebaseFirestore.instance
        .collection("panel")
        .doc(panelId)
        .collection('task')
        .doc(taskId)
        .update(
      {
        "url": null,
        "filename": null,
        "filesize": null,
      },
    );
    print("URL is ");
  }

  Future<void> refreshSubmitModel() async {
    documentFields = null;
    filesize = null;
    sizeInMb = null;
    fileLimit = null;
    isSubmitted = false;
    filepick = false;
    isReviewed = false;
    submittedAt = null;
    fileType = '';
    file = null;
    fileName = '';
    fileExtension = '';
    downloadUrl = '';
    storageReference = null;
    uploadTask = null;
    submitDescription = '';
    // submitDescriptionController = null;
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
