import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/user_model.dart';
import 'package:t_panel_app/screens/globals.dart';

final _firestore = FirebaseFirestore.instance;
final _firestoreUID = FirebaseAuth.instance.currentUser.uid;

class SettingsService {
  deletePanel() async {
    await _firestore.collection('panel').doc(panelId).delete();
  }

  deleteAdmin() async {
    await _firestore.collection('panel').doc(panelId).update({
      'admins': FieldValue.arrayRemove([currentUsername])
    });
  }

  deleteContributor() async {
    print(panelId);
    print(currentUsername);
    await _firestore.collection('panel').doc(panelId).update({
      'participants': FieldValue.arrayRemove([currentUsername])
    });
  }

  Future<Null> getUserDetails(String username) async {
    await _firestore
        .collection('users')
        .where('username', isEqualTo: username)
        .get()
        .then((value) {
      value.docs.forEach((element) {
        userAbout = element.data()['about'];
        experience = element.data()['experience'];
        profession = element.data()['profession'];
        fieldOfStudy = element.data()['field_of_study'];
        linkedin = element.data()['linkedin'];
        github = element.data()['github'];
        email = element.data()['email'];
        profileUrl = element.data()['profile_img_url'];
      });
    });
  }

  Future<Null> addAbout(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'about': valueText});
  }

  Future<Null> addProfession(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'profession': valueText});
  }

  Future<Null> addExperience(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'experience': valueText});
  }

  Future<Null> addFOS(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'field_of_study': valueText});
  }

  Future<Null> addGithub(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'github': valueText});
  }

  Future<Null> addProfileImgUrl() async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'profile_img_url': profileUrl});
  }

  Future<Null> addLinkedin(String valueText) async {
    await _firestore
        .collection('users')
        .doc(_firestoreUID)
        .update({'linkedin': valueText});
  }
}
