import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/services/task_entry_service.dart';

final _firestore = FirebaseFirestore.instance;

class DashboardService {
  Future<DateTime> getPanelUpdate(String panelId) async {
    DateTime lastUpdate = DateTime.utc(1989, DateTime.november, 9);
    await _firestore
        .collection('panel')
        .doc(panelId)
        .collection('task')
        .get()
        .then((value) {
      if (value.size != 0) {
        value.docs.forEach((element) {
          if (lastUpdate.isBefore(element.data()['assigned_at'].toDate())) {
            lastUpdate = element.data()['assigned_at'].toDate();
          }
        });
      }
    });
    return lastUpdate;
  }

  Future<Null> getPanelCreator(panelId) async {
    await _firestore.collection('panel').doc(panelId).get().then((panel) {
      panelCreatorUsername = panel.data()['panel_creator'];
    });
  }

  Future<Null> getUsername() async {
    await _firestore.collection('users').doc(authUser.uid).get().then(
      (value) {
        currentUsername = value.data()['username'];
        print(currentUsername);
      },
    );
  }

  Future<int> getAssignedTask(String panelId) async {
    int assignedTask = 0;
    await _firestore
        .collection('panel')
        .doc(panelId)
        .collection('task')
        .where('assigned_to', arrayContains: currentUsername)
        .get()
        .then((value) {
      assignedTask = value.size;
    });
    return assignedTask;
  }

  Future<int> getOngoingTask(String panelId) async {
    int ongoingTask = 0;
    await _firestore
        .collection('panel')
        .doc(panelId)
        .collection('task')
        .where('assigned_to', arrayContains: currentUsername)
        .where('status', isEqualTo: 'Ongoing')
        .get()
        .then((value) {
      ongoingTask = value.size;
    });
    return ongoingTask;
  }

  Future<int> getCompletedTask(String panelId) async {
    int completedTask = 0;
    await _firestore
        .collection('panel')
        .doc(panelId)
        .collection('task')
        .where('assigned_to', arrayContains: currentUsername)
        .where('status', isEqualTo: 'Completed')
        .get()
        .then((value) {
      completedTask = value.size;
    });
    return completedTask;
  }
}
