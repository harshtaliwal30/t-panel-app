import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/screens/globals.dart';

final _firestore = FirebaseFirestore.instance;

class JoinPanelService {
  Future<bool> joinPanel(
      BuildContext context, TextEditingController panelIdController) async {
    bool flag = false;
    await _firestore
        .collection('panel')
        .where('panelId', isEqualTo: panelIdController.text)
        .get()
        .then(
          (QuerySnapshot querySnapshot) => {
            if (querySnapshot.size == 0)
              {
                print('Panel Not Found'),
              }
            else
              {
                flag = true,
                querySnapshot.docs.forEach((doc) {
                  _firestore.collection('panel').doc(doc.id).update({
                    "participants": FieldValue.arrayUnion([currentUsername]),
                  });
                }),
              }
          },
        );
    return flag;
  }
}
