import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:t_panel_app/models/panel_model.dart';
import 'package:t_panel_app/models/task_model.dart';
import 'package:t_panel_app/screens/globals.dart';
import 'package:t_panel_app/widgets/task/all_tasks.dart';

final firestoreInstance = FirebaseFirestore.instance;
final authUser = FirebaseAuth.instance.currentUser;

class TaskEntryService {
  completedTask() {
    return firestoreInstance
        .collection("panel")
        .doc(panelId)
        .collection("task")
        .where('status', isEqualTo: 'Completed')
        .snapshots();
  }

  incompletedTask() {
    return firestoreInstance
        .collection("panel")
        .doc(panelId)
        .collection("task")
        .where('status', isEqualTo: 'Incompleted')
        .snapshots();
  }

  ongoingTask() {
    return firestoreInstance
        .collection("panel")
        .doc(panelId)
        .collection("task")
        .where('status', isEqualTo: 'Ongoing')
        .snapshots();
  }

  addTask(
    deadline,
    TextEditingController taskNameController,
    _selectedNames,
  ) {
    firestoreInstance.collection("panel").doc(panelId).collection("task").add(
      {
        "task_name": taskNameController.text,
        "deadline": deadline,
        "status": "Ongoing",
        "assigned_to": FieldValue.arrayUnion(_selectedNames),
        "assigned_by": currentUsername,
        "assigned_at": DateTime.now(),
        "isSubmittedLate":isSubmittedLate,
      },
    ).then(
      (value) {
        print(value.id);
      },
    );
  }

  updateTask(
      deadline,taskName, _selectedNames) {
    firestoreInstance
        .collection("panel")
        .doc(panelId)
        .collection("task")
        .doc(taskId)
        .update(
      {
        "task_name": taskName,
        "deadline": deadline,
        "status": status,
        "assigned_to": _selectedNames,
      },
    );
  }

  updateLastUpdated() {
    firestoreInstance.collection("panel").doc(panelId).update(
      {
        'last_updated': DateTime.now(),
      },
    );
  }

  deleteTask() {
    firestoreInstance
        .collection("panel")
        .doc(panelId)
        .collection("task")
        .doc(taskId)
        .delete();
  }

  deleteChat() {
    firestoreInstance.collection('chats').doc(taskId).delete().then((value) {
      print('Chat deleted');
    });
  }

  queryProfile(selectedIndex) {
    return firestoreInstance
        .collection('panel')
        .doc(panelId)
        .collection('task')
        .where('assigned_to', arrayContains: currentUsername)
        .where('status', isEqualTo: taskStatus(selectedIndex))
        .snapshots();
  }
}
