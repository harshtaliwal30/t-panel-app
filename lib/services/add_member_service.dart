import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:t_panel_app/models/panel_model.dart';

final _firestore = FirebaseFirestore.instance;

class AddMemberService {
  Future<List> getUsers() async {
    List users = [];
    await _firestore.collection('users').get().then(
      (QuerySnapshot query) {
        if (query.size != 0) {
          query.docs.forEach((doc) {
            users.add(doc['username']);
          });
        } else {
          print('No users found');
        }
      },
    );
    return users;
  }

  createAdmin(String contributor) async {
    await _firestore.collection('panel').doc(panelId).update({
      'admins': FieldValue.arrayUnion([contributor])
    });
  }

  deleteAdmin(String contributor) async {
    await _firestore.collection('panel').doc(panelId).update({
      'admins': FieldValue.arrayRemove([contributor])
    });
  }

  createContributor(List _selectedNames) {
    _firestore.collection('panel').doc(panelId).update({
      'participants': FieldValue.arrayUnion(_selectedNames),
    });
  }

  deleteContributor(String contributor) async {
    await _firestore.collection('panel').doc(panelId).update({
      'participants': FieldValue.arrayRemove([contributor])
    });
  }
}
