import 'package:cloud_firestore/cloud_firestore.dart';

final _firestore = FirebaseFirestore.instance;

class CreatePanelService {
  Future<List> getPanel(String panelId) async {
    List details = [];
    await _firestore
        .collection('panel')
        .doc(panelId)
        .get()
        .then((DocumentSnapshot panelSnap) {
      details.add(panelSnap.data()['panel_name']);
      details.add(panelSnap.data()['purpose']);
      details.add(panelSnap.data()['description']);
    });
    return details;
  }

  Future<bool> postPanel({
    String panelName,
    String description,
    String purpose,
    String panelCreator,
    FieldValue admins,
    FieldValue participants,
    String panelId,
  }) async {
    Map<String, dynamic> data = {
      "panel_name": panelName,
      "description": description,
      "purpose": purpose,
      "panel_creator": panelCreator,
      "admins": admins,
      "panelId": panelId,
      "participants": participants,
      "last_updated": DateTime.now(),
    };

    await _firestore.collection("panel").add(data).then((value) {
      print(value);
    });

    return true;
  }

  Future<bool> updatePanel(
      {String panelName,
      String description,
      String purpose,
      String panelId}) async {
    Map<String, dynamic> data = {
      "panel_name": panelName,
      "description": description,
      "purpose": purpose,
    };

    await _firestore.collection("panel").doc(panelId).update(data);

    return true;
  }
}
