import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:t_panel_app/screens/globals.dart';

final _firestore = FirebaseFirestore.instance;

class ChatService {
  Future<Null> sendMessage(String taskDocId, String enteredMessage) async {
    await _firestore.collection('chats').doc(taskDocId).collection('chat').add({
      'text': enteredMessage,
      'messaged_at': Timestamp.now(),
      'username': currentUsername
    });
  }

  Future<Null> deleteMessage(String taskDocId, String chatDocId) async {
    await _firestore
        .collection('chats')
        .doc(taskDocId)
        .collection('chat')
        .doc(chatDocId)
        .delete();
  }
}
