import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:t_panel_app/screens/globals.dart';

final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final authUser = FirebaseAuth.instance.currentUser;

class NotificationService {
  Future<Null> postDeviceToken({String fcmToken}) async {
    print(currentUsername);
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    Map<String, dynamic> data = {
      "deviceToken": fcmToken,
      "createdAt": FieldValue.serverTimestamp(),
      "platform": Platform.operatingSystem,
      "androidId": androidInfo.androidId,
      "username": FieldValue.arrayUnion([currentUsername]),
    };

    await _firestore.collection('tokens').doc(androidInfo.androidId).set(data);
  }

  Future<List<String>> getFCMTokens(List usernames) async {
    List<String> list = [];
    await _firestore
        .collection('tokens')
        .where('username', arrayContainsAny: usernames)
        .get()
        .then((tokens) {
      tokens.docs.forEach((token) {
        list.add(token.data()['deviceToken']);
      });
    });
    return list;
  }

  Future<Null> pushNotification(
      {@required String title,
      @required String msg,
      @required List sendTo,
      @required String screen}) async {
    List<String> tokens = await getFCMTokens(sendTo);
    final Map<String, dynamic> body = {
      //"to": token,
      "registration_ids": tokens,
      "data": {"screen": screen},
      "notification": {
        "title": title,
        "body": msg,
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
      }
    };

    try {
      final http.Response response = await http.post(
          'https://fcm.googleapis.com/fcm/send',
          body: json.encode(body),
          headers: <String, String>{
            'authorization':
                "key=AAAAtBY0y88:APA91bG_mm1_qY3mPotz6qKJ0C8PIdndvjYDgIvPyG56W30fiNk9Z1kVwMsdMAqFLMXIX4sGob8VJrVPTUXSro4HsILGBDt_rgF4LK4oVZlR9RZhEIy8ahlAnvbTTnY6O7f-TjfsHzeW",
            'Content-Type': 'application/json'
          });

      if (response.statusCode == 200) {
        print('Notification Send Successfully');
        final Map<String, dynamic> res = json.decode(response.body);

        int success = res['success'];
        print("Success status is: ");
        print(success);
      } else {
        print('Invalid status code: ${response.statusCode}');
      }
    } catch (err) {
      print(err);
    }
  }

  Future<bool> postNotification(
      {String title, String msg, String link, String date}) async {
    Map<String, dynamic> data = {
      "title": "hello",
      "msg": "testing",
      "link": "gvg",
      "date": "",
    };

    await _firestore.collection("/notifications").add(data).then((value) {
      print(value);
    });
    return true;
  }
}
